#include "form.h"
#include "ui_form.h"
#include "widget.h"
#include<QMessageBox>

Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);

    getlist(getList);
    /*timer = new QTimer;
    connect(timer, SIGNAL(timeout()), this, SLOT(getlist()));
    timer->start(10000);*/
}

Form::~Form()
{
    delete ui;
}

void Form::getlist(QStringList &getList)
{
    getList.clear();
    qDebug()<<"into getlist"<<endl;
    /*QProcess process2;
    process2.start("search_wifi.sh");
    process2.waitForFinished(-1);
    process2.close();*/
    QFile link_txt("./net_data.txt");
    if(!link_txt.open(QIODevice::ReadOnly))
    {

    }

    //分割WiFi名
    QByteArray byte_arr = link_txt.readAll();
    QString str = QString(byte_arr);
    //QStringList getList;
    QStringList lineList = str.split("\n");
    qDebug() << lineList.length();
    for(int i = 1; i <lineList.length() && lineList.at(i) != Q_NULLPTR; i++)
    {
        QString tmp = lineList.at(i);
        int lastBlank = tmp.lastIndexOf('\t'); //找到\t最后出现的位置--返回值int
        char del = '\\';
        if(del == tmp[lastBlank + 1])
        {
             continue;
        }
        getList.append(tmp.mid(lastBlank + 1));//指定位置截取字符,+1跳过\t
    }
    ui->comboBox->addItems(getList);
    link_txt.close();
}
void Form::on_pushButton_clicked()
{
    ui->pushButton_13->setDisabled(true);
    QString temp = "./link_wifi.sh";
    QFile file(temp);
    if(!file.open(QIODevice::ReadWrite|QIODevice::Truncate))
    {

    }
    QByteArray rrr("#!/bin/bash\n");
    file.write(rrr);

    QString a = "'";
    QString str = "wpa_cli -i wlan0 set_network 1 ssid " + a + '"' + ui->comboBox->currentText() + '"' + a + '\n';
    QByteArray arr1(str.toUtf8());
    file.write(arr1);

    QString str2 = "wpa_cli -i wlan0 set_network 1 psk " + a + '"' + ui->lineEdit->text() + '"' + a + '\n';
    QByteArray arr2(str2.toUtf8());
    file.write(arr2);

    QByteArray arr3("wpa_cli -i wlan0 select_network 1 \n");
    file.write(arr3);

    QByteArray arr4("udhcpc -i wlan0\n");
    file.write(arr4);

    str = "echo ";
    QString str3 = str + '"' + "nameserver 114.114.114.114" + '"' + "> /etc/resolv.conf" + '\n';
    QByteArray arr5(str3.toUtf8());
    file.write(arr5);

    QString str4 = str + '"' + "nameserver 8.8.8.8" + '"' + ">> /etc/resolv.conf" + '\n';
    QByteArray arr6(str4.toUtf8());
    file.write(arr6);

    QString str5 = str + '"' +"xiaolongshigou" + '"'+'\n';
    QByteArray arr7(str5.toUtf8());
    file.write(arr7);

    /*QProcess process;
    process.start("link_wifi.sh");//启动进程
    process.waitForFinished(-1);
    process.close();*/
    ui->pushButton->setDisabled(true);
    ui->pushButton_13->setEnabled(true);
    file.close();
}
void Form::on_pushButton_13_clicked()
{
    this->close();
}
void Form::on_pushButton_2_clicked()
{
    ui->lineEdit->clear();
}

void Form::on_pushButton_3_clicked()
{
    ui->lineEdit->insert("1");
}

void Form::on_pushButton_4_clicked()
{
    ui->lineEdit->insert("2");
}

void Form::on_pushButton_5_clicked()
{
    ui->lineEdit->insert("3");
}

void Form::on_pushButton_6_clicked()
{
    ui->lineEdit->insert("4");
}

void Form::on_pushButton_7_clicked()
{
    ui->lineEdit->insert("5");
}

void Form::on_pushButton_8_clicked()
{
    ui->lineEdit->insert("6");
}

void Form::on_pushButton_9_clicked()
{
    ui->lineEdit->insert("7");
}

void Form::on_pushButton_10_clicked()
{
    ui->lineEdit->insert("8");
}

void Form::on_pushButton_11_clicked()
{
    ui->lineEdit->insert("9");
}

void Form::on_pushButton_12_clicked()
{
    ui->lineEdit->insert("0");
}



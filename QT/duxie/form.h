#ifndef FORM_H
#define FORM_H

#include <QWidget>

namespace Ui {
class Form;
}

class Form : public QWidget
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = nullptr);
    ~Form();

protected:
    void getlist(QStringList &getList);
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();
    //void getlist();
    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_10_clicked();

    void on_pushButton_11_clicked();

    void on_pushButton_12_clicked();


    void on_pushButton_13_clicked();

private:
    Ui::Form *ui;
    QTimer *timer;
    QStringList getList;
};

#endif // FORM_H

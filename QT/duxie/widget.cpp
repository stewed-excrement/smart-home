#include "widget.h"
#include "ui_widget.h"
#include<QByteArray>
#include<QFile>
#include<QDebug>
#include<QTimer>
#include<QDateTime>
#include<QPixmap>
#include<QPalette>
#include<QBrush>
#include<QUrlQuery>
#include<QNetworkConfigurationManager>
#include<QMessageBox>
#include<QProcess>
#include<QComboBox>
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    num = 1;
    _socket = new QTcpSocket;//默认父对象为当前窗体
    /*_socket->connectToHost(QHostAddress("119.91.109.180"),8344);//客户端连接服务端
    QByteArray arr("cmd=1&uid=ea036b159e5741339de6b095b6b143c5&topic=light2\r\n");
    _socket->write(arr);
    connect(_socket,SIGNAL(readyRead()),this,SLOT(recv_info()));//关联接收*/
    //QPixmap pixmap = QPixmap(":/1.jpg");
   // QPalette pa(this->palette());
   // pa.setBrush(QPalette::Background,QBrush(pixmap.scaled(this->size(),Qt::IgnoreAspectRatio,
                                                  //        Qt::SmoothTransformation)));
    //this->setPalette(pa);
    this->setWindowFlags(Qt::FramelessWindowHint);//不用边框

    ui->pushButton_3->setStyleSheet("QPushButton{background-image: url(:/2.jpg);}"

                                    "QPushButton:pressed{background-image:url(:/4.jpg);}");
    ui->pushButton_4->setStyleSheet("QPushButton{background-image: url(:/4.jpg);}"

                                    "QPushButton:pressed{background-image:url(:/2.jpg);}");
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(showtime()));
    ui->lcdNumber->setDigitCount(19);
    ui->lcdNumber->setSegmentStyle(QLCDNumber::Filled);
    ui->lcdNumber->setStyleSheet("border:7px solid red;color:red;"
                                 "background:pink");


    ui->shiduTLb->installEventFilter(this);
    m_startAngle=60;
    m_endAngle=60;
    m_minValue=0;
    m_maxValue=100;
    m_scaleMajor = 10;//分度
    m_scaleMinor = 10;

    shidu="50";
    /* 事件过滤 */
    ui->shiduTLb->installEventFilter(this); // 启用事件过滤器

    timer2 = new QTimer(this);
    connect( timer2, SIGNAL(timeout()), this, SLOT(updateshidu()) );
    timer2->start(1000);

    m_width = 20;
    maxValue = 40;
    minValue = -30;
    m_radius = 1.05;
    m_value = -25;
    curValue = m_value;
    QTimer *at = new QTimer(this);
    at->start(1000);
    m_valueAnimation = new QPropertyAnimation(this, "value");
    m_valueAnimation->setDuration(1000);
    m_valueAnimation->setEasingCurve(QEasingCurve::OutCubic);
    m_valueAnimation->setLoopCount(1);
    connect(at, SIGNAL(timeout()), this, SLOT(startAnimation()));
    timer3 = new QTimer(this);

    connect(timer3, SIGNAL(timeout()), this, SLOT(watchnet()));
    timer3->start(1000);
}

Widget::~Widget()
{
    delete ui;
}
void Widget::watchnet()
{

    if(!mgr.isOnline())
    {
            ui->pushButton->setEnabled(true);
            ui->pushButton->setStyleSheet("QPushButton{background-image: url(:/11.jpg);}");
            QFile file("link_wifi.sh");
            file.open(QIODevice::ReadOnly);
            if(file.size() > 241)
            {
                QProcess process;
                process.start("link_wifi.sh");//启动进程
                process.waitForFinished(-1);
                process.close();

            }
            QProcess process2;
            process2.start("search_wifi.sh");
            process2.waitForFinished(-1);
            process2.close();
    }
    else if(mgr.isOnline())
    {
          ui->pushButton->setDisabled(true);
          ui->pushButton->setStyleSheet("QPushButton{background-image: url(:/22.jpg);}");
          //_socket = new QTcpSocket;//默认父对象为当前窗体
          _socket->connectToHost(QHostAddress("119.91.109.180"),8344);//客户端连接服务端
          QByteArray arr("cmd=1&uid=ea036b159e5741339de6b095b6b143c5&topic=light2\r\n");
          _socket->write(arr);
          connect(_socket,SIGNAL(readyRead()),this,SLOT(recv_info()));//关联接收
          while(num == 1)
          {
              QTcpSocket *socket = new QTcpSocket;
              socket->connectToHost(QHostAddress("128.138.141.172"), 13);
              if(socket->waitForConnected())
              {
                  if(socket->waitForReadyRead())
                  {
                      QString str(socket->readAll());
                       qDebug() << "str is:"<<str;
                      str = str.trimmed();
                      str = str.section(" ", 1, 2);
                      socket->close();
                        qDebug() << "str is:"<<str;

                        bbb = str;
                        setTime(bbb);
                  }
              }
              QProcess process3;
              QString a ="date -s ";
              QString b = a + '"' + bbb + '"';
              process3.startDetached(b);
              process3.startDetached("hwclock -w");// 同步系统时间
              process3.startDetached("sync");// 同步系统时间
              num--;
              timer->start(1000);
          }
    }
}
void Widget::setTime(QString &str1)
{
        QStringList list1 = str1.split(" ");
        str1  = list1.at(1);
        QStringList list2 = str1.split(":");
        str1 = list2.at(0);
        int hour = str1.toInt() + 8;
        if(hour >= 24)
        {
            hour = hour - 24;
        }
         str1 = "20"+list1.at(0) + " "+QString("%d").number(hour)+ ":"+list2.at(1)+":"+list2.at(2);
}
void  Widget::updateshidu()
{
    int hum_raw = 0;
    int hum_offset = 0;
    float hum_scale = 0;


    QString device = "iio:device0";
    QString filename = "in_humidityrelative_raw";
    read_sysfs_int(device, filename, &hum_raw);

    filename =  "in_humidityrelative_offset";
    read_sysfs_int(device, filename, &hum_offset);

    filename =  "in_humidityrelative_scale";
    read_sysfs_float(device, filename, & hum_scale);

    int a;
    a = static_cast<int>((hum_raw + hum_offset) * hum_scale / 1000);
    QString arr = QString("%d").number(a);
    shidu =arr;
    QString str = "cmd=2&uid=ea036b159e5741339de6b095b6b143c5&topic=shidu&msg="+arr+"\r\n";
   _socket->write(str.toUtf8());
    update();
}

bool Widget::eventFilter(QObject *watched, QEvent *event)
{

    if (watched == ui->shiduTLb && event->type() == QEvent::Paint)
    {
        drawSecondHand1();
    }
    return QWidget::eventFilter(watched,event);
}
void Widget::paintEvent(QPaintEvent *e)
{
    m_rect.setX(0);
    m_rect.setY(20 - height()/2);
    m_rect.setWidth(m_width);
    m_rect.setHeight(height()/2 - 40 - m_width* m_radius);
    QPainter painter(this);
    QPen pen(Qt::black);
    painter.translate(this->width()/1.2, this->height());  //坐标轴移动到中心点
    painter.setRenderHints(QPainter::TextAntialiasing | QPainter::Antialiasing);  // 启用反锯齿
    painter.save();
    //绘制上方的柱状
    painter.fillRect(m_rect, QColor(168,200, 225));

    //绘制底部的圆
    QRectF tmpRect = QRectF(m_rect.bottomLeft(), QPointF(m_width, height()/40- m_width*m_radius -20));
    painter.fillRect(tmpRect, QColor(255, 0, 0));
    painter.setPen(Qt::NoPen);
    painter.setBrush(QColor(255, 0, 0));
    painter.drawEllipse(tmpRect.bottomLeft()+QPointF(tmpRect.width()/2, 0),m_width*m_radius, m_width*m_radius);
    painter.restore();

    //绘制刻度以及刻度值
     painter.save();
     painter.setPen(QColor(Qt::green));
     int nYCount = (maxValue - minValue)/10+1;
     qreal perHeight = (m_rect.height())/nYCount;//数字旁边的那根长线
     for (int i=0; i<nYCount; ++i)
     {
         QPointF basePoint = m_rect.bottomLeft() - QPointF(0, perHeight/2) - QPointF(0, perHeight*i);
         //左侧大刻度
         painter.drawLine(basePoint, basePoint+QPointF(-10, 0));
         for (int j=1; j<10; ++j)
         {
             if(i == nYCount -1)
             continue;
             painter.drawLine(basePoint-QPointF(0, perHeight/10*j),basePoint-QPointF(5, perHeight/10*j));
         }
         painter.drawText(basePoint+QPointF(-40, 10), QString("%1").arg(minValue+i*10));
         //右侧大刻度
         basePoint = m_rect.bottomRight() - QPointF(0, perHeight/2) - QPointF(0, perHeight*i);
         painter.drawLine(basePoint, basePoint+QPointF(10, 0));
         for (int j=1; j<10; ++j)
         {
             if(i == nYCount -1)
                continue;
             painter.drawLine(basePoint-QPointF(0, perHeight/10*j),basePoint-QPointF(-5, perHeight/10*j));
         }
     }
        painter.restore();

        //根据值填充m_rect
        qreal h = (m_value-minValue)/(maxValue-minValue)*(m_rect.height()-perHeight);
        if(h<0)
            h = 0;
        if(h > m_rect.height())
            h = m_rect.height();
        painter.fillRect(m_rect.adjusted(0, m_rect.height()-h-perHeight/2-1 , 0, 0), QColor(255, 0, 0));
       QWidget::paintEvent(e);
}

void Widget::startAnimation()
{
    qreal startValue = getValue();
    //QString arr = QString("%d").arg(m_value);
    //ui->textEdit->setText(arr);
     QString brr = QString("%d").number(m_value);
     if(m_value >= 30)//temperature start fengshan
         {
             QFile file("/sys/class/hwmon/hwmon1/pwm1");
             file.open(QIODevice::WriteOnly | QIODevice::Truncate);
             QString str = "200";
             file.write(str.toUtf8());
         }
         else
         {
             QFile file("/sys/class/hwmon/hwmon1/pwm1");
             file.open(QIODevice::WriteOnly | QIODevice::Truncate);
             QString str = "0";
             file.write(str.toUtf8());
         }

     QString str = "cmd=2&uid=ea036b159e5741339de6b095b6b143c5&topic=wendu&msg="+brr+"\r\n";
    _socket->write(str.toUtf8());
    m_valueAnimation->setKeyValueAt(0, startValue-1);
    m_valueAnimation->setKeyValueAt(0.5, curValue+1);
    m_valueAnimation->setKeyValueAt(1, curValue);
    m_valueAnimation->setStartValue(startValue-2);
    m_valueAnimation->start();
}

qreal Widget::getValue()
{
    int temp_raw = 0;
    int temp_offset = 0;
    float temp_scale = 0;
    QString device = "iio:device0";
    QString filename = "in_temp_raw";
    read_sysfs_int(device, filename, &temp_raw);

    filename = "in_temp_offset";
    read_sysfs_int(device, filename, &temp_offset);

    filename =  "in_temp_scale";
    read_sysfs_float(device, filename, &temp_scale);

    m_value = ((temp_raw + temp_offset) * temp_scale / 1000);
    return m_value;
}
int Widget::read_sysfs_float(QString &device, QString &filename, float *val)
{
    QString temp = "/sys/bus/iio/devices/"+device+"/"+filename;
    QFile file(temp);
    if(!file.open(QIODevice::ReadOnly))
    {

    }
    QByteArray arr = file.readAll();
    *val = arr.toFloat();
    return 0;
}
int Widget::read_sysfs_int(QString &device, QString &filename, int *val)
{
    QString temp = "/sys/bus/iio/devices/"+device+"/"+filename;
    QFile file(temp);
    if(!file.open(QIODevice::ReadOnly))
    {

    }
    QByteArray arr = file.readAll();
    *val = arr.toInt();

    return 0;
}

void Widget::drawSecondHand1()
{
    QPainter painter(ui->shiduTLb);
    painter.drawPixmap(pixmap2.rect(), pixmap2);
    QFont font("Microsoft Yahei", 6, 35); // 字体，大小，加粗等同于QFont::Bold
    painter.setFont(font);
    painter.setRenderHint(QPainter::Antialiasing, true);  // 反锯齿
    int side = qMin(ui->shiduTLb->width(), ui->shiduTLb->height());//获取窗体的宽和高的函数首字母是小写


    /* 灰色表盘背景 */
    painter.save();
    painter.setPen(Qt::NoPen); // 去掉外圈线
    painter.setBrush(QColor(239, 241, 240, 120));              // 背景颜色以及透明度
    painter.drawEllipse( QPoint(width()/4, height()/7), side/2-24, side/2-24 ); // 绘制背景 灰色背景圈大小 画圆
    painter.restore();

    /* 刻度表盘外框 */
    painter.save();
    painter.setPen(QPen( QColor(11, 22, 44, 130), 14 )); // 外边框颜色以及大小
    painter.drawEllipse(QPoint(width()/4 + 4, height()/7 + 6), side/2 - 35, side/2 - 35); //外边框绘制
    painter.restore();
    painter.translate(width()/4 +  4, height()/7 + 6);         // 坐标原点
    painter.scale(side / 180.0, side / 180.0);            // 缩放比例 //刻度盘的大小


    /* 中心点 */
    painter.setBrush(QBrush(QColor(56,189,96)));
    painter.drawEllipse(QPoint(0, 0), 6, 6);

    /*指针*/
    painter.save();
    painter.rotate(m_startAngle);
    double degRotate =  (360.0 - m_startAngle - m_endAngle)/(m_maxValue - m_minValue)*(shidu.toInt()- m_minValue);
    painter.rotate(degRotate);
    pen.setColor(QColor(56,189,96));
    pen.setWidth(1);
    painter.setPen(pen);
    painter.drawLine(0,70,0,-20);
    painter.restore();

    /* Lcdnumber风格绘制 */
    painter.save();

    if (!m_fontList.isEmpty())
    {
        QFont font;
        font.setFamily(m_fontList.at(0));
        font.setPointSize(15);
        painter.setFont(font);
    }
    painter.setPen( QColor(0, 0, 0) );
    painter.drawText( -40, 24, 80, 12, Qt::AlignCenter,shidu+"%" );
    painter.drawText( -40, 34, 80, 22, Qt::AlignCenter,"湿度计" );
    painter.restore();

    /*数字刻度*/
    painter.save();
    painter.setPen(Qt::black);
    //m_startAngle是起始角度，m_endAngle是结束角度，m_scaleMajor在一个量程中分成的刻度数
    double startRad = ( 270-m_startAngle) * (3.14 / 180);
    double deltaRad = (360 - m_startAngle - m_endAngle) * (3.14 / 180) / m_scaleMajor;
    double sina,cosa;
    int x, y;
    QFontMetricsF fm(this->font());
    double w, h, tmpVal;
    QString str;

    for (int i = 0; i <= m_scaleMajor; i++)
    {
        sina = sin(startRad - i * deltaRad);
        cosa = cos(startRad - i * deltaRad);

        tmpVal = 1.0 * i *((m_maxValue - m_minValue) / m_scaleMajor) + m_minValue;
        // tmpVal = 50;
        str = QString( "%1" ).arg(tmpVal);  //%1作为占位符   arg()函数比起 sprintf()来是类型安全的
        w = fm.size(Qt::TextSingleLine,str).width();
        h = fm.size(Qt::TextSingleLine,str).height();
        x = 55 * cosa - w / 2 + 2;
        y = -55 * sina + h / 4 + 5;
        painter.drawText(x, y, str); //函数的前两个参数是显示的刻度数字坐标位置，后一个是显示的内容，是字符类型""
    }
        painter.restore();

        /*刻度线*/
        painter.save();
        painter.rotate(m_startAngle);
        int steps = (m_scaleMajor * m_scaleMinor); //相乘后的值是分的份数
        double angleStep = (360.0 - m_startAngle - m_endAngle) / steps; //每一个份数的角度
        QPen pen ;
        pen.setColor(Qt::black);  //推荐使用第二种方式
        for (int i = 0; i <= steps; i++)
        {
            if (i % m_scaleMinor == 0)//整数刻度显示加粗
            {
                 pen.setWidth(1);             //设置线宽
                 painter.setPen(pen);   //使用面向对象的思想，把画笔关联上画家。通过画家画出来
          painter.drawLine(0, 62, 0, 72); //两个参数应该是两个坐标值
           }
           else
           {
               pen.setWidth(0);
               painter.setPen(pen);
               painter.drawLine(0, 67, 0, 72);
           }
           painter.rotate(angleStep);//旋转角度
        }
        painter.restore();//恢复绘图器状态
}

void Widget::showtime()
{
    QDateTime dt = QDateTime::currentDateTime();
    QString str = dt.toString("yyyy-MM-dd-hh:mm:ss");
    ui->lcdNumber->display(str);
}

void Widget::on_pushButton_3_clicked()
{
    QFile file1("/sys/class/leds/led1/brightness");
    QFile file2("/sys/class/leds/led2/brightness");
    QFile file3("/sys/class/leds/led3/brightness");
    QString s1 = "1";
    file1.open(QIODevice::WriteOnly | QIODevice::Truncate);
    file2.open(QIODevice::WriteOnly | QIODevice::Truncate);
    file3.open(QIODevice::WriteOnly | QIODevice::Truncate);
    file1.write(s1.toUtf8());
    file2.write(s1.toUtf8());
    file3.write(s1.toUtf8());
    file1.close();
    file2.close();
    file3.close();
    QByteArray arr("cmd=2&uid=ea036b159e5741339de6b095b6b143c5&topic=light&msg=led_on\r\n");
    _socket->write(arr);
}

void Widget::on_pushButton_4_clicked()
{
    QFile file1("/sys/class/leds/led1/brightness");
    QFile file2("/sys/class/leds/led2/brightness");
    QFile file3("/sys/class/leds/led3/brightness");
    QString s2 = "0";
    file1.open(QIODevice::WriteOnly | QIODevice::Truncate);

    file2.open(QIODevice::WriteOnly | QIODevice::Truncate);


    file3.open(QIODevice::WriteOnly | QIODevice::Truncate);
    file1.write(s2.toUtf8());
    file2.write(s2.toUtf8());
    file3.write(s2.toUtf8());

    file1.close();
    file2.close();
    file3.close();
    QByteArray arr("cmd=2&uid=ea036b159e5741339de6b095b6b143c5&topic=light&msg=led_off\r\n");
    _socket->write(arr);
}
void Widget::recv_info()
{
    QByteArray arr("cmd=1&uid=ea036b159e5741339de6b095b6b143c5&topic=light2\r\n");
    _socket->write(arr);
    QByteArray buf = _socket->readAll();
    QString str(buf);
    QUrlQuery q(str);
    const QString msg = q.queryItemValue("msg");
    QString brr = msg.section("%",0,0);
    if(brr.isEmpty())
    {
        return;
    }
    if(brr == "led_on")
    {
        ui->pushButton_3->click();
        qDebug()<<"into bafayun"<<brr<<endl;
        //on_pushButton_3_clicked();
    }
    else if(brr == "led_off")
    {
        ui->pushButton_4->click();
        qDebug()<<"into bafayun"<<brr<<endl;
        //on_pushButton_4_clicked();
    }
    else
    {
        return ;
    }

}

/*
void Widget::on_pushButton_4_clicked()
{
    ui->textEdit->clear();
    QByteArray arr("cmd=2&uid=1213ba0c33d4409488aed2c07e1c63e6&topic=dunshi&msg=led_off\r\n");
    _socket->write(arr);
}*/

void Widget::on_pushButton_clicked()
{
    Form *form = new Form;
    form->show();
}

#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include<QFileDialog>
#include<QString>
#include<QtNetwork>
#include<QPen>
#include<QPainter>
#include <QPropertyAnimation>
namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    qreal getValue();
protected:
    bool eventFilter(QObject *watched, QEvent *event);

    void drawSecondHand1();
    void drawSecondHand2();

    void paintEvent(QPaintEvent *e);
    int read_sysfs_float(QString &device, QString &filename,float *val);
    int read_sysfs_int(QString &device, QString &filename,int *val);
private slots:
    void showtime();
    void on_pushButton_4_clicked();
    void on_pushButton_3_clicked();
    void updateshidu();
    void startAnimation();
    void recv_info();
private:
    Ui::Widget *ui;
    QStringList m_fontList;
    QPen pen;
    QTimer *timer;
    QTimer *timer2;
    QTcpSocket *_socket;
    QPixmap pixmap2;
    QPixmap pixmap3;
    int m_maxValue;
    int m_minValue;
    int m_startAngle;
    int m_endAngle;
    int m_scaleMajor;
    int m_scaleMinor;
    QString shidu;
    qreal m_value;//double
    qreal curValue;
    int m_width;
    QRectF m_rect;
    int maxValue, minValue;
    qreal m_radius;
    QPropertyAnimation *m_valueAnimation;

};

#endif // WIDGET_H
